use atomicwrites::{AllowOverwrite, AtomicFile};
use clap::Clap;
use core::str::FromStr;
use failure::{bail, format_err, Error};
use governor::{Quota, RateLimiter};
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
use itertools::Itertools;
use kv::{Bucket, Key, Value};
use log::{debug, error, info, warn};
use mime2ext::mime2ext;
use pretty_env_logger::env_logger;
use reqwest::{
    blocking::{Client, Request},
    Method, Url,
};
use scraper::{ElementRef, Html, Selector};
use serde::{Deserialize, Serialize};
use std::{
    convert::TryFrom,
    fs, io,
    io::Write,
    mem,
    num::NonZeroU32,
    path::{Path, PathBuf},
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc, Mutex,
    },
    thread,
    time::Duration,
};

static CACHE_VERSION: u8 = 0;

struct WriteAdapter {
    sender: Sender<u8>,
}

impl io::Write for WriteAdapter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        for chr in buf {
            self.sender.send(*chr).unwrap();
        }
        Ok(buf.len())
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

struct ProgressBarHandler {
    no_progress_bar: bool,
    progress_bar_spinner: Option<indicatif::ProgressBar>,
    progress_bar_next: Option<indicatif::ProgressBar>,
    progress_bar_images: Option<indicatif::ProgressBar>,
    still_writing: Option<Arc<Mutex<()>>>,
}

impl ProgressBarHandler {
    fn new(
        no_progress_bar: bool,
        logs_tx: Option<Receiver<u8>>,
        tasks: &Bucket<UrlBinWrapper, Task>,
        incompletable_tasks: &[Url],
    ) -> Self {
        assert!(
            no_progress_bar == logs_tx.is_none(),
            "There should be no logs receiver if there is no progressbar"
        );

        if no_progress_bar {
            Self {
                no_progress_bar: true,
                progress_bar_spinner: None,
                progress_bar_next: None,
                progress_bar_images: None,
                still_writing: None,
            }
        } else {
            // Count tasks of differend types and completion levels
            let (next_length, next_position, images_length, images_position) = tasks
                .iter()
                .map(|result| {
                    let item = result.unwrap();
                    (item.key::<Url>().unwrap(), item.value::<Task>().unwrap())
                })
                .fold((0, 0, 0, 0), |mut acc, item| {
                    match item.1.r#type {
                        TaskType::Next => {
                            acc.0 += 1;
                            if item.1.completed || incompletable_tasks.contains(&item.0) {
                                acc.1 += 1;
                            }
                        }
                        TaskType::Image => {
                            acc.2 += 1;
                            if item.1.completed || incompletable_tasks.contains(&item.0) {
                                acc.3 += 1;
                            }
                        }
                    }

                    acc
                });

            // Create progress bars
            let progress_bar_style = ProgressStyle::default_bar()
                .template("{prefix}▕{bar:.40}▏ {pos}/{len} {msg}")
                .progress_chars("█▉▊▋▌▍▎▏ ");

            let progress_bar_main = MultiProgress::new();

            let progress_bar_spinner = progress_bar_main.add(
                ProgressBar::new_spinner().with_style(
                    ProgressStyle::default_spinner()
                        .template(" {spinner} [{elapsed_precise}] {msg}"),
                ),
            );

            let progress_bar_next = progress_bar_main.add(
                ProgressBar::new(next_length)
                    .with_style(progress_bar_style.clone())
                    .with_position(next_position),
            );
            progress_bar_next.set_prefix("links queried    ");

            let progress_bar_images = progress_bar_main.add(
                ProgressBar::new(images_length)
                    .with_style(progress_bar_style)
                    .with_position(images_position),
            );
            progress_bar_images.set_prefix("images downloaded");

            let still_writing = Arc::new(Mutex::new(()));

            {
                let progress_bar_spinner = progress_bar_spinner.clone();
                let still_writing = still_writing.clone();
                let logs_tx = logs_tx.unwrap();

                thread::spawn(move || {
                    let mut buffer = vec![];

                    loop {
                        progress_bar_spinner.tick();

                        // Lock while writing, so that the program only exits when we are done here
                        // TODO This lock is not very pretty
                        let lock = still_writing.lock().unwrap();
                        thread::sleep(Duration::from_secs_f64(0.1));

                        while let Ok(log_char) = logs_tx.try_recv() {
                            buffer.push(log_char);

                            if buffer.last() == Some(&"\n".as_bytes()[0]) {
                                progress_bar_spinner.println(String::from_utf8_lossy(
                                    &buffer.drain(..).collect::<Vec<u8>>(),
                                ));
                            }
                        }

                        mem::drop(lock);

                        if progress_bar_spinner.is_finished() {
                            break;
                        }
                    }
                });
            }

            thread::spawn(move || progress_bar_main.join().unwrap());

            Self {
                no_progress_bar: false,
                progress_bar_spinner: Some(progress_bar_spinner),
                progress_bar_next: Some(progress_bar_next),
                progress_bar_images: Some(progress_bar_images),
                still_writing: Some(still_writing),
            }
        }
    }

    fn set_message(&self, msg: String) {
        if !self.no_progress_bar {
            self.progress_bar_spinner.as_ref().unwrap().set_message(msg)
        }
    }

    fn set_message_next(&self, msg: String) {
        if !self.no_progress_bar {
            self.progress_bar_next.as_ref().unwrap().set_message(msg)
        }
    }

    fn set_message_images(&self, msg: String) {
        if !self.no_progress_bar {
            self.progress_bar_images.as_ref().unwrap().set_message(msg)
        }
    }

    fn update_positions(
        &self,
        next_length: u64,
        next_position: u64,
        images_length: u64,
        images_position: u64,
    ) {
        if !self.no_progress_bar {
            self.progress_bar_next
                .as_ref()
                .unwrap()
                .inc_length(next_length);
            self.progress_bar_next.as_ref().unwrap().inc(next_position);
            self.progress_bar_images
                .as_ref()
                .unwrap()
                .inc_length(images_length);
            self.progress_bar_images
                .as_ref()
                .unwrap()
                .inc(images_position);
        }
    }

    fn finish(self) {
        if !self.no_progress_bar {
            // wait for the log writing before we are done
            let _lock = self.still_writing.as_ref().unwrap().lock().unwrap();

            self.progress_bar_spinner.unwrap().finish();
            self.progress_bar_next.unwrap().finish();
            self.progress_bar_images.unwrap().finish();
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
struct Task {
    r#type: TaskType,
    index: Vec<usize>,
    completed: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
enum TaskType {
    Image,
    Next,
}

impl Value for Task {
    fn to_raw_value(&self) -> Result<kv::Raw, kv::Error> {
        Ok(bincode::serialize(self)
            .map_err(|error| kv::Error::Message(error.to_string()))?
            .into())
    }

    fn from_raw_value(r: kv::Raw) -> Result<Self, kv::Error> {
        bincode::deserialize(r.as_ref()).map_err(|error| kv::Error::Message(error.to_string()))
    }
}

#[derive(Debug, Clone)]
struct UrlBinWrapper {
    url: Url,
    bin: Vec<u8>,
}

impl From<Vec<u8>> for UrlBinWrapper {
    // This is mostly for generating empty prefixes, don't use for anything else!
    fn from(buf: Vec<u8>) -> Self {
        UrlBinWrapper {
            bin: buf,
            url: Url::parse("http://example.com/").unwrap(),
        }
    }
}

impl From<Url> for UrlBinWrapper {
    fn from(url: Url) -> Self {
        UrlBinWrapper {
            bin: bincode::serialize(url.as_str()).unwrap(),
            url,
        }
    }
}

impl TryFrom<&str> for UrlBinWrapper {
    type Error = Error;

    fn try_from(url: &str) -> Result<Self, Error> {
        let url = Url::from_str(url)?;

        Ok(UrlBinWrapper {
            bin: bincode::serialize(url.as_str()).unwrap(),
            url,
        })
    }
}

impl Into<Url> for UrlBinWrapper {
    fn into(self) -> Url {
        self.url
    }
}

impl AsRef<[u8]> for UrlBinWrapper {
    fn as_ref(&self) -> &[u8] {
        self.bin.as_ref()
    }
}

impl<'a> Key<'a> for UrlBinWrapper {
    fn from_raw_key(x: &kv::Raw) -> Result<Self, kv::Error> {
        Ok(UrlBinWrapper {
            url: Url::parse(
                &bincode::deserialize::<String>(x.as_ref())
                    .map_err(|error| kv::Error::Message(error.to_string()))?,
            )
            .map_err(|error| kv::Error::Message(error.to_string()))?,
            bin: x.to_vec(),
        })
    }
}

fn gen_name(url: &Url, mime: Option<&str>, index: &[usize], use_index: bool) -> String {
    format!(
        "{index}{filename}{ext}",
        index = {
            if use_index {
                format!(
                    "{} - ",
                    index
                        .iter()
                        .map(|index| index.to_string())
                        .intersperse(String::from("."))
                        .collect::<String>()
                )
            } else {
                String::from("")
            }
        },
        filename = Path::new(url.path())
            .file_stem()
            .map(|filename| filename.to_str().unwrap())
            .unwrap_or("index"),
        ext = match mime {
            None => String::from(""),
            Some(mime) => match mime2ext(mime) {
                Some(ext) => format!(".{}", {
                    if ext == "jpeg" {
                        "jpg"
                    } else {
                        ext
                    }
                }),
                None => {
                    warn!("Unable to find extention for {} ({})", mime, url);
                    String::from("")
                }
            },
        }
    )
}

#[derive(Clap, Debug)]
#[clap(name = env!("CARGO_PKG_NAME"), about = "Simple lightweight webcomic scraper", version = env!("CARGO_PKG_VERSION"))]
struct Args {
    #[clap(
        about = "Start page from where the scraper will begin seraching for images and next links",
        required = true,
        takes_value = true,
        parse(try_from_str)
    )]
    start_url: Url,
    #[clap(
        about = "CSS Selector for the images (`<img>` with `src` or `srcset`) on the page",
        required = true,
        takes_value = true,
        parse(try_from_str)
    )]
    images_selector: SelectorWrapper,
    #[clap(
        about = "CSS Selector for the next links (`<a>` with `href`) on the page",
        required = true,
        takes_value = true,
        parse(try_from_str)
    )]
    nexts_selector: SelectorWrapper,
    #[clap(
        about = "Directory to save images (and cache and html) to",
        required = true,
        takes_value = true,
        parse(from_os_str)
    )]
    out_dir: PathBuf,
    #[clap(
        about = "Will not only save the images, but also the html as files",
        short,
        long
    )]
    save_html: bool,
    #[clap(
        about = "Normaly all file names are prefixed to preserve their order and keep them from beeing overwritten, this disables this",
        short = 'i',
        long
    )]
    no_index: bool,
    #[clap(
        about = "Max fails for one request until giving up",
        short = 't',
        long,
        takes_value = true,
        default_value = "6"
    )]
    max_tries: usize,
    #[clap(
        about = "How many requests per minute are allowed",
        short = 'l',
        long,
        takes_value = true,
        default_value = "120"
    )]
    rate_limit: NonZeroU32,
    #[clap(
        about = "Enables verbose output, you can also use RUST_LOG env var to set the log level",
        short,
        long
    )]
    verbose: bool,
    #[clap(
        about = "Useful when using an archive site as start. This disables the next link search on any site other than the first (the archive site). This will also force to always scan the first page, even if it is cached",
        short = 'a',
        long
    )]
    from_archive: bool,
    #[clap(
        about = "Disables the progress bar (is automatically disabled when no shell is detected)",
        short = 'p',
        long
    )]
    no_progress_bar: bool,
    #[clap(
        about = "Disables checking if an image is already downloaded",
        short = 'o',
        long
    )]
    always_overwrite: bool,
    // TODO Add image link regex rewrite
}

#[derive(Debug, Clone)]
struct SelectorWrapper(Selector);

impl FromStr for SelectorWrapper {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        Ok(Self(
            Selector::parse(input).map_err(|error| format_err!("{:?}", error))?,
        ))
    }
}

impl Into<Selector> for SelectorWrapper {
    fn into(self) -> Selector {
        self.0
    }
}

fn main() -> Result<(), Error> {
    let args = Args::parse();

    // Spawn logging pipe
    let logs_tx = {
        let (target, logs_tx) = {
            if args.no_progress_bar {
                (env_logger::Target::Stderr, None)
            } else {
                let (rx, tx) = channel();
                (
                    env_logger::Target::Pipe(Box::new(WriteAdapter { sender: rx })),
                    Some(tx),
                )
            }
        };

        pretty_env_logger::formatted_timed_builder()
            .target(target)
            .parse_env(
                env_logger::Env::new()
                    .default_write_style_or("always")
                    .default_filter_or(if args.verbose {
                        format!("info,{}=debug", env!("CARGO_PKG_NAME"))
                    } else {
                        String::from("info")
                    }),
            )
            .init();

        logs_tx
    };

    debug!("{:#?}", args);

    let nexts_selector: Selector = args.nexts_selector.clone().into();
    let images_selector: Selector = args.images_selector.clone().into();

    // Rate limiter to limit requests
    let limiter = RateLimiter::direct(
        Quota::per_minute(args.rate_limit).allow_burst(NonZeroU32::new(1).unwrap()),
    );

    // Reqwest Client to keep sockets open
    let client = Client::new();

    fs::create_dir_all(&args.out_dir)?;

    // Open and check the sledge db
    let tasks = {
        let corrupted_cache_msg = "Corrupted cache, please delete the `.cache` file.";

        let is_new_cache = args.out_dir.join(".cache").exists();
        let cache = kv::Store::new(kv::Config::new(&args.out_dir.join(".cache"))).unwrap();

        let general = cache
            .bucket::<kv::Raw, Vec<u8>>(Some("general"))
            .expect(&corrupted_cache_msg);
        if is_new_cache {
            let file_version_raw = general.get("version").unwrap().expect(&corrupted_cache_msg);
            let file_version = file_version_raw.get(0).expect(&corrupted_cache_msg);

            if file_version != &CACHE_VERSION {
                bail!("Mismatching cache version ({}: {}, `.cache`: {}), please delete the `.cache` file.",
                    env!("CARGO_PKG_NAME"), CACHE_VERSION, file_version);
            }
        } else {
            general.set("version", vec![CACHE_VERSION]).unwrap();
        }

        cache
            .bucket::<UrlBinWrapper, Task>(Some("tasks"))
            .expect(&corrupted_cache_msg)
    };

    debug!(
        "{:#?}",
        tasks
            .iter()
            .map(|e| {
                let e = e.unwrap();
                (
                    {
                        let url: Url = e.key().unwrap();
                        url.to_string()
                    },
                    e.value().unwrap(),
                )
            })
            .collect::<Vec<(String, Task)>>()
    );

    // List of incompletable tasks, this is used to determine when there are no more tasks that are able to complete
    let incompletable_tasks: Mutex<Vec<Url>> = Mutex::new(vec![]);

    // Prepate tasks list
    if !tasks.contains(args.start_url.clone()).unwrap() {
        tasks
            .set(
                args.start_url.clone(),
                Task {
                    r#type: TaskType::Next,
                    index: 
                        if args.from_archive {
                            vec![]
                        } else {
                            vec![0]
                        },
                    completed: false,
                },
            )
            .unwrap();
    } else if args.from_archive {
        // If we use an archive page we have to set the task for the archive page to not complete
        tasks
            .set(args.start_url.clone(), {
                let mut start_task = tasks.get(args.start_url.clone()).unwrap().unwrap();
                start_task.completed = false;
                start_task
            })
            .unwrap();
    }

    // Spawn progressbar
    let progressbar_handler = ProgressBarHandler::new(
        args.no_progress_bar,
        logs_tx,
        &tasks,
        &incompletable_tasks.lock().unwrap(),
    );

    // Creates infinite iterator of incomplete tasks, will only be empty if there are no incomplete (without uncompletable) tasks left
    let tasks_iter = tasks
        .iter()
        .map(|result| {
            let item = result.unwrap();
            (item.key::<Url>().unwrap(), item.value::<Task>().unwrap())
        })
        .filter(|item| !item.1.completed && !incompletable_tasks.lock().unwrap().contains(&item.0))
        .cycle();

    // Main loop for processing tasks
    for (url, mut task) in tasks_iter {
        if !task.completed {
            debug!("{}:\n{:#?}", url, task);

            progressbar_handler.set_message(url.clone().into());

            // We expect the task to complete, else this flag will be unset or not saved
            task.completed = true;

            // Download the task url
            match || -> Result<Vec<u8>, Error> {
                // Check if the file was already downloaded
                // Ignores HTML files, because it's very possible they might have changed
                if !args.always_overwrite && !matches!(task.r#type, TaskType::Next) {
                    let tmp_name = gen_name(&url, None, &task.index, !args.no_index);

                    if let Some(path) = fs::read_dir(&args.out_dir)
                        .unwrap()
                        .map(|path| path.unwrap().path())
                        .filter(|path| {
                            if let Some(extension) = path.extension() {
                                extension != "html"
                            } else {
                                false
                            }
                        })
                        .find(|path| path.file_stem().unwrap().to_str().unwrap() == tmp_name)
                    {
                        info!("Already downloaded {}", path.to_str().unwrap());

                        return Ok(fs::read(path)?);
                    }
                }

                let mut delay: u64 = 1;
                for _ in 0..args.max_tries {
                    while limiter.check().is_err() {
                        thread::sleep(Duration::from_secs_f64(0.1));
                    }

                    let resp = client.execute(Request::new(Method::GET, url.clone()));
                    if let Ok(resp) = resp {
                        if resp.status().is_success() {
                            return Ok(resp.bytes()?.to_vec());
                        } else if resp.status().is_client_error() {
                            task.completed = false;
                            bail!(
                                "Request {} failed with status code {} (Will not try again)",
                                url,
                                resp.status()
                            );
                        } else {
                            warn!(
                                "Request {} failed with status code {} (Will wait {}sec before next try)",
                                url,
                                resp.status(),
                                delay
                            );
                        }
                    } else {
                        warn!(
                            "Request {} failed: {} (Will wait {}sec before next try)",
                            url,
                            resp.unwrap_err(),
                            delay
                        );
                    }
                    thread::sleep(Duration::from_secs(delay));
                    delay *= 2;
                }

                task.completed = false;
                bail!("Max tries reached while requesting {}", url);
            }() {
                Ok(body) => {
                    // Handle request content
                    match task.r#type {
                        TaskType::Next => {
                            progressbar_handler.set_message_next(url.clone().into());

                            if args.save_html {
                                // This uses atomic writes so that incomplete files are not possible
                                let af = AtomicFile::new(
                                    &args.out_dir.join(gen_name(
                                        &url,
                                        Some("text/html"),
                                        &task.index,
                                        !args.no_index,
                                    )),
                                    AllowOverwrite,
                                );
                                af.write(|f| f.write_all(&body))?;
                            }

                            let html = Html::parse_document(&String::from_utf8_lossy(&body));

                            if !args.from_archive || url == args.start_url {
                                // if flag from-archive is not present or first task
                                let next_links: Vec<ElementRef> =
                                    html.select(&nexts_selector).collect();
                                if next_links.is_empty() {
                                    warn!("Found no next link on {}", url.as_str());
                                    task.completed = false;
                                }
                                for (i, link) in next_links.iter().enumerate() {
                                    if let Some(link_url) = link.value().attr("href") {
                                        let link_url =
                                            UrlBinWrapper::try_from(url.join(link_url)?).unwrap();

                                        if !tasks.contains(link_url.clone()).unwrap() {
                                            tasks
                                                .set(
                                                    link_url,
                                                    Task {
                                                        r#type: TaskType::Next,
                                                        index: {
                                                            let mut index = task.index.clone();
                                                            if args.from_archive {
                                                                index = vec![i];
                                                            } else if next_links.len() > 1 {
                                                                index.push(i);
                                                            } else {
                                                                *{ index.last_mut().unwrap() } += 1;
                                                            }
                                                            index
                                                        },
                                                        completed: false,
                                                    },
                                                )
                                                .unwrap();

                                            progressbar_handler.update_positions(1, 0, 0, 0);
                                        }
                                    } else {
                                        warn!(
                                            "Found a next link without a href (on {}): {}",
                                            url.as_str(),
                                            link.html()
                                        );
                                        task.completed = false;
                                    }
                                }
                            }

                            if !args.from_archive || url != args.start_url {
                                // Don't search the archive page for images
                                let images: Vec<ElementRef> =
                                    html.select(&images_selector).collect();
                                if images.is_empty() {
                                    warn!("Found no image on {}", url.as_str());
                                    task.completed = false;
                                }
                                for (i, image) in images.iter().enumerate() {
                                    if let Some(image_url) = image.value().attr("src") {
                                        let image_url =
                                            UrlBinWrapper::try_from(url.join(image_url)?).unwrap();

                                        if !tasks.contains(image_url.clone()).unwrap() {
                                            tasks
                                                .set(
                                                    image_url,
                                                    Task {
                                                        r#type: TaskType::Image,
                                                        index: {
                                                            let mut index = task.index.clone();
                                                            if images.len() > 1 {
                                                                index.push(i);
                                                            }
                                                            index
                                                        },
                                                        completed: false,
                                                    },
                                                )
                                                .unwrap();

                                            progressbar_handler.update_positions(0, 0, 1, 0);
                                        }
                                    } else {
                                        // TODO implement srcset search
                                        warn!(
                                            "Found a image without a src (on {}): {}",
                                            url.as_str(),
                                            image.html()
                                        );
                                        task.completed = false;
                                    }
                                }
                            }

                            progressbar_handler.update_positions(0, 1, 0, 0);
                        }
                        TaskType::Image => {
                            progressbar_handler.set_message_images(url.clone().into());

                            // This uses atomic writes so that incomplete files are not possible
                            let af = AtomicFile::new(
                                &args.out_dir.join(gen_name(
                                    &url,
                                    Some(&tree_magic::from_u8(&body)),
                                    &task.index,
                                    !args.no_index,
                                )),
                                AllowOverwrite,
                            );
                            af.write(|f| f.write_all(&body))?;

                            progressbar_handler.update_positions(0, 0, 0, 1);
                        }
                    }
                }
                Err(err) => error!("{}", err),
            }

            // If task did not complete it will probably not complete in this run
            if !task.completed {
                incompletable_tasks.lock().unwrap().push(url.clone());
            }

            // Update task in db
            tasks.set(UrlBinWrapper::from(url), task).unwrap();
            tasks.flush().unwrap();
        }
    }

    info!("Done");

    // Finish progressbar
    progressbar_handler.finish();

    Ok(())
}
