# Rusty webcomic scraper
## Basics
The scraper needs at least 3 arguments, the url where to start scraping from, the CSS selector for the next button(s) and the selector for the image(s) on the site. Then it will jump from site to site and dowload the images.
## Examples
- [Sequential Art](https://www.collectedcurios.com/sequentialart.php)
  ```
  $ rustycomicscraper "https://www.collectedcurios.com/sequentialart.php?s=1" ".w3-display-middle > .w3-image" "#forwardOne" out_sequentialart
  ```
- [Twokinds](ttps://twokinds.keenspot.com/) (uses archive mode: starts from the archive site and only searches for next links on the first site)
  ```
  $ rustycomicscraper "https://twokinds.keenspot.com/archive/" ".comic img" '.chapter-links > a' out_twokinds -a
  ```
- [Freefall]() (also archive mode)
  ```
  $ rustycomicscraper "http://freefall.purrsia.com/ffdex.htm" "img[width]" 'a[href*="/ff"]:not([href$=".gif"]):not([href$=".png"]):not([href$=".jpg"]):not([href$=".txt"])' out_freefall -a
  ```
